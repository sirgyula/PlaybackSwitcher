﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace PlaybackSwitcher
{
	class PlaybackSwitcherClass
	{
		System.Windows.Forms.NotifyIcon notifyIcon;
		Dictionary<string, int> devices;
		List<string> cmdOutput;
		string lastDevice = "";
		string monitor = "LG Ultra HD";
		string headphone = "Speakers (Realtek High Definition Audio)";
		string monitorIco = "ico_speaker.ico";
		string headphoneIco = "ico_headphone.ico";

		public PlaybackSwitcherClass()
		{
			notifyIcon = new System.Windows.Forms.NotifyIcon();
			devices = new Dictionary<string, int>();
			cmdOutput = new List<string>();

			createNotifyIcon();

			getDevices();
			switchPlaybackDevice(headphone);
		}

		public void createNotifyIcon()
		{
			notifyIcon.Icon = new System.Drawing.Icon(headphoneIco);
			notifyIcon.Text = monitor;
			notifyIcon.Visible = true;
			notifyIcon.Click += notifyIcon_Click;
		}

		public void getDevices()
		{
			runCommandInPrompt("CppPlaybackSwitcher.exe", true);

			foreach (var item in cmdOutput)
			{
				if (item.Contains(monitor))
				{
					devices.Add(monitor, Convert.ToInt32(item[13].ToString()));
				}
				else if (item.Contains(headphone))
				{
					devices.Add(headphone, Convert.ToInt32(item[13].ToString()));
				}
			}
		}

		public void switchPlaybackDevice(string device)
		{
			runCommandInPrompt("CppPlaybackSwitcher.exe " + devices[device].ToString());
			lastDevice = device;
		}

		public void openSoundMixer()
		{
			runCommandInPrompt("sndvol.exe -a 58984200");
		}

		public void runCommandInPrompt(string command, bool printOutput = false)
		{
			System.Diagnostics.Process process = new System.Diagnostics.Process();
			System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
			startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
			startInfo.FileName = "cmd.exe";
			startInfo.Arguments = "/C " + command;
			startInfo.UseShellExecute = false;
			startInfo.RedirectStandardOutput = true;
			startInfo.CreateNoWindow = true;
			process.StartInfo = startInfo;
			process.Start();

			if (printOutput)
			{
				cmdOutput.Clear();

				while (!process.StandardOutput.EndOfStream)
				{
					string line = process.StandardOutput.ReadLine();
					cmdOutput.Add(line);
					System.Diagnostics.Debug.WriteLine(line);
				}
			}

			process.Close();
		}

		private void notifyIcon_Click(object sender, System.EventArgs e)
		{
			MouseEventArgs ee = (MouseEventArgs)e;

			if (ee.Button == MouseButtons.Left)
			{
				if (lastDevice.Equals(monitor))
				{
					switchPlaybackDevice(headphone);
					notifyIcon.Icon = new System.Drawing.Icon(headphoneIco);
					notifyIcon.Text = headphone;
				}
				else
				{
					switchPlaybackDevice(monitor);
					notifyIcon.Icon = new System.Drawing.Icon(monitorIco);
					notifyIcon.Text = monitor;
				}
			}
			else if (ee.Button == MouseButtons.Right)
			{
				openSoundMixer();
			}
		}
	}
}
